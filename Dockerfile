FROM bellsoft/liberica-openjdk-alpine:17
EXPOSE 8080
ADD /build/libs/credit-helper*.jar /usr/local/credit-helper/credit-helper.jar
ENTRYPOINT ["java", "-Dapp.home=/usr/src/credit-helper/home", "-jar", "/usr/local/credit-helper/credit-helper.jar"]
