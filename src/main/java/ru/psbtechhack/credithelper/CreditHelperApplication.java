package ru.psbtechhack.credithelper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CreditHelperApplication {

	public static void main(String[] args) {
		SpringApplication.run(CreditHelperApplication.class, args);
	}

}
